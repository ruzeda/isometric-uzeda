#include <base64.h>
#include <atitc.h>

#ifndef H_ISOMETRIC_SCENE
	#define H_ISOMETRIC_SCENE

	#include "cocos2d.h"
	#include "db/DatabaseManager.h"
	#include "dialog/DialogManager.h"
	#include "entity/Entity.h"

class IsometricMap;

class IsometricScene : public cocos2d::Scene
{
public:
	CREATE_FUNC(IsometricScene);

	static IsometricScene* createScene(Entity* player, const std::string& layerName, const cocos2d::Vec2& coord);
	bool init() override;
	void update(float delta) override;
	void setPlayer(Entity* player);

	IsometricMap* getMap() const { return _map; }

protected:
	short _speedX = 0;
	short _speedY = 0;
	IsometricMap* _map = nullptr;
	cocos2d::Vec2 _mapStartPosition;
	Entity* _player = nullptr;
	SpriteLayer* _playerLayer = nullptr;
	cocos2d::Vector<Entity*> _entities;
	std::unique_ptr<DialogManager> _dialogManager;
	std::unique_ptr<DatabaseManager> _databaseManager;
	cocos2d::Node* _dialogContainer = nullptr;

	void cameraMove();
	void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, __unused cocos2d::Event* event);
	void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, __unused cocos2d::Event* event);
	bool onTouchBegan(cocos2d::Touch* touch, __unused cocos2d::Event* event);
	void onTouchEnded(__unused cocos2d::Touch* touch, __unused cocos2d::Event* event);

	// TODO move this to SpriteLayer
	cocos2d::Vec2 tileFromPosition(const cocos2d::Vec2& pWorld);
	static std::vector<SpriteTile*>
	findPath(const cocos2d::Vec2& startCoord, const cocos2d::Vec2& targetCoord, SpriteLayer* layer);
	static void validateCandidateForOpenList(
		SpriteTile* inAnalysis, SpriteTile* current, const cocos2d::Vec2& targetCoord, std::vector<SpriteTile*>& open);
	void togglePlayerControl(const bool value);
	void startDialog();
};

#endif // H_ISOMETRIC_SCENE
