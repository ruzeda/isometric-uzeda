#include "Entity.h"
#include "map/IsometricMap.h"
#include "map/SpriteLayer.h"
#include "map/SpriteBatchLayer.h"
#include <iomanip>
#include <utility>

USING_NS_CC;

Entity::Entity() : Sprite() {}

bool Entity::init(const std::string& textureIdentifier)
{
	CCLOG("Initializing %d Entity - %s", _ID, textureIdentifier.c_str());
	_identifier = textureIdentifier;
	_direction = "down";

	CCLOG("SpriteFrameCache - %s.plist", _identifier.c_str());
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("sprites/" + _identifier + ".plist");

	std::string frameNames = _direction + "_still/x.png";
	CCLOG("Get animation - %s", frameNames.c_str());
	auto still = getAnimation(frameNames, 1);

	CCLOG("Init with sprite frame");
	if (!initWithSpriteFrame(still.front()))
	{
		return false;
	}

	setAnchorPoint(Vec2(0.5f, 0.25));
	/*auto anchorPoint = DrawNode::create();
	anchorPoint->drawDot(Point(
		getContentSize().width * getAnchorPoint().x,
		getContentSize().height * getAnchorPoint().y),
		3, Color4F(1.0, 0.0, 0.0, 1.0));
	addChild(anchorPoint);*/
	CCLOG("%s initialized", _identifier.c_str());
	retain();
	return true;
}

void Entity::move(int x, int y)
{
	SpriteTile* targetTile = nullptr;
	if (!_moving)
	{
		if (!_currentPath.empty())
		{
			targetTile = _currentPath.front();
			_currentPath.erase(_currentPath.begin());
			moveToTile(targetTile);
		}

		if (targetTile == nullptr && (x != 0 || y != 0))
		{
			Vec2 targetCoord = _currentTile->getCoords() + Vec2(static_cast<float>(x), static_cast<float>(y));
			if (targetCoord.x >= 0 && targetCoord.y >= 0 && targetCoord.x < _layer->getLayerSize().width &&
				targetCoord.y < _layer->getLayerSize().height)
			{
				moveToTile(_layer->getTileAt(targetCoord));
			}
		}
	}
}

void Entity::moveToTile(SpriteTile* targetTile)
{
	if (targetTile != nullptr)
	{
		const Vec2 delta = targetTile->getCoords() - _currentTile->getCoords();
		int x = static_cast<int>(delta.x);
		int y = static_cast<int>(delta.y);
		const int z = static_cast<int>(targetTile->getLayerChange().z);

		if (z != 0 || targetTile->isWalkableInto())
		{
			stopAllActions();
			_moving = true;

			Vec2 deltaPos = (targetTile->getSpriteAnchor() - _currentTile->getSpriteAnchor()) / 2;
			MoveBy* move = MoveBy::create(0.125f * static_cast<float>(std::abs(x) + std::abs(y)), deltaPos);

			setDirection(x, y);

			Vector<SpriteFrame*> frames = getAnimation(_direction + "/x.png", 2);
			auto animation = Animation::createWithSpriteFrames(frames, 0.15f);

			CallFunc* endMovement = CallFunc::create(CC_CALLBACK_0(Entity::onMovementEnd, this, targetTile));
			Sequence* sequence;
			if (x < 0 || y < 0 || x == y)
			{
				auto updateZOrder = CallFunc::create(CC_CALLBACK_0(Entity::updateLocalZOrder, this, targetTile));
				sequence = Sequence::create(move, updateZOrder, move, endMovement, NULL);
			}
			else
				sequence = Sequence::create(move, move, endMovement, NULL);

			runAction(RepeatForever::create(Animate::create(animation)));
			runAction(sequence);
		}
		else
			_currentPath.clear();
	}
}

void Entity::setDirection(const int& x, const int& y)
{
	if (x == 0 && y == 0)
	{
		_direction = "down";
		return;
	}

	if (y > 0 && x == 0) _direction = "downleft";
	else if (y < 0 && x == 0) _direction = "upright";
	else if (y == 0 && x < 0) _direction = "upleft";
	else if (y == 0 && x > 0) _direction = "downright";
	else if (y < 0 && x < 0) _direction = "up";
	else if (y < 0 && x > 0) _direction = "right";
	else if (y > 0 && x < 0) _direction = "left";
	else if (y > 0 && x > 0) _direction = "down";
}

void Entity::updateLocalZOrder(SpriteTile const* targetTile)
{
	setLocalZOrder(targetTile->getLocalZOrder());
}

void Entity::onMovementEnd(SpriteTile* targetTile)
{
	stopAllActions();
	_currentTile = targetTile;
	updateLocalZOrder(targetTile);

	if (targetTile->getLayerChange().z != 0)
	{
		changeLayer(targetTile);
	}

	_moving = false;
	auto still = getAnimation(_direction + "_still/x.png", 1);
	auto animation = Animation::createWithSpriteFrames(still, 1.0f / 5);
	runAction(RepeatForever::create(Animate::create(animation)));
}

void Entity::changeLayer(const SpriteTile* layerChangingTile)
{
	const Vec3 layerChange = layerChangingTile->getLayerChange();

	const int z = _layer->getLocalZOrder() + static_cast<int>(layerChange.z);
	if (z >= 1 && z < _layer->getMap()->getChildren().size())
	{
		auto layerNode = _layer->getMap()->getChildren().at(z);
		auto layer = dynamic_cast<SpriteLayer*>(layerNode);
		if (layer != nullptr)
		{
			_layer->removeChild(this);
			_layer = layer;
			Vec2 targetCoord =
				layerChangingTile->getCoords() + Vec2(layerChange.x - layerChange.z, layerChange.y - layerChange.z);
			_layer->addEntity(this, targetCoord);
			setDirection(static_cast<int>(layerChange.x), static_cast<int>(layerChange.y));
		}
	}
}

Vector<SpriteFrame*> Entity::getAnimation(const std::string& frameNames, const int& count)
{
	auto spriteCache = SpriteFrameCache::getInstance();
	Vector<SpriteFrame*> animFrames;
	if (spriteCache != nullptr)
	{
		std::stringstream ss;
		std::string frameName;
		for (int i = 1; i <= count; i++)
		{
			frameName = std::string(frameNames);
			ss.str("");
			ss.clear();
			ss << std::setw(3) << std::setfill('0') << i;
			frameName.replace(frameName.length() - 5, 1, ss.str());
			animFrames.pushBack(spriteCache->getSpriteFrameByName(frameName));
		}
	}
	return animFrames;
}
