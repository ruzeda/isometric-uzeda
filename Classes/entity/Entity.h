#ifndef H_ENTITY
#define H_ENTITY

#include "cocos2d.h"
#include "map/SpriteTile.h"

class SpriteLayer;

class Entity : public cocos2d::Sprite
{
public:
	Entity();

	/**
	 * Initializes an entity with the sprite sheet under "Resources/{textureIdentifier}.plist" (from TexturePacker).
	 * @param textureIdentifier the name of the PLIST file under Resources.
	 * @return true if the initialization doesn't throw any exceptions.
	 */
	bool init(const std::string& textureIdentifier);

	/**
	 * Moves this entity in its isometric map horizontally by x tiles and vertically by y tiles (consider x, y as speed
	 * since this ignores pathfinding and moves the entity in a single "move").
	 * @param x number of tiles this entity should move by horizontally.
	 * @param y number of tiles this entity should move by vertically.
	 */
	void move(int x, int y);

	/**
	 * Updates this entity's localZOrder to the tile's localZOrder.
	 * @param targetTile the tile to get the localZOrder from.
	 */
	void updateLocalZOrder(SpriteTile const* targetTile);

	SpriteTile* getCurrentTile() const noexcept { return _currentTile; }

	void setCurrentTile(SpriteTile* tile) noexcept { _currentTile = tile; }

	cocos2d::Vec2 getStartPosition() const noexcept { return _startPosition; };

	void setStartPosition(const cocos2d::Vec2& pos) noexcept { _startPosition = pos; }

	SpriteLayer* getLayer() const noexcept { return _layer; }

	void setLayer(SpriteLayer* layer) noexcept { _layer = layer; }

	const std::string& getIdentifier() const { return _identifier; }

	bool isMoving() const noexcept { return _moving; }

	void move(const std::vector<SpriteTile*>& path) { _currentPath = path; }

protected:
	SpriteTile* _currentTile = nullptr;
	cocos2d::Vec2 _startPosition;
	SpriteLayer* _layer = nullptr;
	std::string _identifier = "";
	std::string _direction = "";
	bool _moving = false;
	std::vector<SpriteTile*> _currentPath;

	/**
	 * Moves this entity to the target tile's coordinates in a single "move".
	 * @param targetTile the tile this entity should be moved to.
	 */
	void moveToTile(SpriteTile* targetTile);

	/**
	 * Calculates the direction string that composes the name of the sprite's animation in it's sprite sheet.
	 * @param x the horizontal speed at which this entity is moving at.
	 * @param y the vertical speed at which this entity is moving at.
	 */
	void setDirection(const int& x, const int& y);

	/**
	 * Retrieves count frames from the collection of texture frames of the given animation from this sprite's sprite
	 * sheet.
	 * @param frameNames the name of the desired animation.
	 * @param count the number of frames to retrieve.
	 * @return the list of texture frames.
	 */
	static cocos2d::Vector<cocos2d::SpriteFrame*> getAnimation(const std::string& frameNames, const int& count);

	/**
	 * Moves this sprite from its current layer's Z index in the isometric map by the number declared in the "z" TMX
	 * property of layerChangingTile.
	 * @param layerChangingTile the tile containing layer change TMX properties.
	 */
	void changeLayer(const SpriteTile* layerChangingTile);

	/**
	 * Event listener callback for the end of the moving animation.
	 * @param targetTile the targetTile this entity has moved to.
	 */
	void onMovementEnd(SpriteTile* targetTile);
};

#endif