/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "Constants.h"
#include "dialog/control/TextInput.h"
#include "dialog/DialogManager.h"
#include "IsometricScene.h"
#include "map/IsometricMap.h"
#include "map/SpriteBatchLayer.h"
#include "map/SpriteLayer.h"
#include "map/SpriteTile.h"

USING_NS_CC;

IsometricScene* IsometricScene::createScene(Entity* player, const std::string& layerName, const Vec2& coord)
{
	CCLOG("Creating isometric scene with player starting on %s (%.0f, %.0f)", layerName.c_str(), coord.x, coord.y);
	IsometricScene* scene = IsometricScene::create();
	scene->retain();

	auto layer = dynamic_cast<SpriteLayer*> (scene->getMap()->getSpriteLayer(layerName));
	layer->addEntity(player, coord);
	scene->setPlayer(player);
	return scene;
}

// Print useful error message instead of segfaulting when files are not there.
__unused static void problemLoading(const char* filename) noexcept
{
	CCLOGERROR("Error while loading: %s\n", filename);
	CCLOGERROR(
		"Depending on how you compiled you might have to add 'Resources/' in front of filenames in IsometricScene.cpp\n");
}

bool IsometricScene::init()
{
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	setPosition(Vec2::ZERO);

	CCLOG("GAME INITIALIZATION START");
	_map = new IsometricMap();
	_map->init("maps/isometric.tmx", this);
	addChild(_map);

	_dialogContainer = Node::create();
	_dialogContainer->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	addChild(_dialogContainer);
	_dialogManager = std::unique_ptr<DialogManager>(new DialogManager(_dialogContainer));

	_databaseManager = std::unique_ptr<DatabaseManager>(new DatabaseManager());
	_databaseManager->open();
	_databaseManager->close();

	auto layer = dynamic_cast<SpriteLayer*>(_map->getSpriteLayer("hill"));
	auto entity = new Entity();
	entity->init("player");
	layer->addEntity(entity, Vec2(13, 5));
	_entities.pushBack(entity);

	/*auto layer = dynamic_cast<SpriteLayer*>(_map->getSpriteLayer("earth"));
	for (int x = 0; x < 30; x+=1)
	{
		for (int y = 0; y < 20; y+=1)
		{
			if (layer->getTileIdAt(Vec2(x, y)) == 0)
			{
				auto entity = new Entity("player");
				layer->addEntity(entity, Vec2(x, y));
				_entities.push_back(entity);
			}
		}
	}*/

	auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(IsometricScene::onKeyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(IsometricScene::onKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(keyboardListener, this);

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan =
		CC_CALLBACK_2(IsometricScene::onTouchBegan, this);
	touchListener->onTouchEnded =
		CC_CALLBACK_2(IsometricScene::onTouchEnded, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);

	scheduleUpdate();
	return true;
}

void IsometricScene::setPlayer(Entity* player)
{
	_player = player;
	_playerLayer = player->getLayer();

	Vec2 pos = Vec2::ZERO;
	pos.subtract(Vec2(player->getPosition().x, player->getPosition().y));

	const uint width = static_cast<uint>(Director::getInstance()->getVisibleSize().width);
	const uint height = static_cast<uint>(Director::getInstance()->getVisibleSize().height);
	pos.add(Vec2(width >> 1u, height >> 1u));

	_map->setPosition(pos);
	_mapStartPosition = pos;
}

void IsometricScene::startDialog()
{
	auto uiDialog = _dialogManager->createUIDialog(Size(24, 6));
	uiDialog->setPosition(_player->getPosition());

	auto widget = new TextInput();
	widget->init("Placeholder", 11);
	uiDialog->addChild(widget);

	/*auto dialog = _dialogManager->createTextDialog(Size(24, 3), _player);
	dialog->showText("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent mattis massa "
									 "augue, quis venenatis ante feugiat sed. Maecenas semper tellus id sagittis "
									 "tempus. Nunc at luctus nulla. Etiam ullamcorper risus libero. Nam pretium "
									 "mollis lacus, a faucibus tortor volutpat at. Vivamus laoreet orci sed dolor "
									 "lacinia, eget varius elit volutpat. Aenean dui ipsum, dictum ac nibh sit amet, "
									 "finibus placerat est. Nunc ac nunc tortor. Nullam eget lacus eu lorem mattis "
									 "mollis eu sed dui. Phasellus tempor volutpat massa eget malesuada. Nam eget "
									 "nisi et lorem tristique imperdiet. Mauris facilisis nunc eros. Nullam "
									 "dignissim dictum est. Morbi porta bibendum consequat. In hac habitasse platea "
									 "dictumst. Integer sodales, nunc vitae varius eleifend, ipsum turpis volutpat "
									 "nisi, et congue nisl risus at nibh.");*/
}

void IsometricScene::update(float delta)
{
	_player->move(_speedX, _speedY);
	cameraMove();

	for (auto entity : _entities)
		entity->move(_speedX * -1, _speedY * -1);
}

void IsometricScene::cameraMove()
{
	if (_playerLayer != _player->getLayer())
		setPlayer(_player);

	Vec2 mapPos = _mapStartPosition;
	mapPos.add(_player->getStartPosition());
	mapPos.subtract(_player->getPosition());
	_map->setPosition(mapPos);
	_dialogContainer->setPosition(mapPos);
	_dialogManager->update();
}

void IsometricScene::onKeyPressed(EventKeyboard::KeyCode keyCode, __unused Event* event)
{
	switch (keyCode)
	{
		case EventKeyboard::KeyCode::KEY_W:
		case EventKeyboard::KeyCode::KEY_UP_ARROW: _speedY = -1;
			break;

		case EventKeyboard::KeyCode::KEY_A:
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW: _speedX = -1;
			break;

		case EventKeyboard::KeyCode::KEY_S:
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW: _speedY = 1;
			break;

		case EventKeyboard::KeyCode::KEY_D:
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW: _speedX = 1;
			break;

		default: break;
	}
}

void IsometricScene::onKeyReleased(EventKeyboard::KeyCode keyCode, __unused Event* event)
{
	switch (keyCode)
	{
		case EventKeyboard::KeyCode::KEY_W:
		case EventKeyboard::KeyCode::KEY_UP_ARROW: _speedY = 0;
			break;

		case EventKeyboard::KeyCode::KEY_A:
		case EventKeyboard::KeyCode::KEY_LEFT_ARROW: _speedX = 0;
			break;

		case EventKeyboard::KeyCode::KEY_S:
		case EventKeyboard::KeyCode::KEY_DOWN_ARROW: _speedY = 0;
			break;

		case EventKeyboard::KeyCode::KEY_D:
		case EventKeyboard::KeyCode::KEY_RIGHT_ARROW: _speedX = 0;
			break;

		default: break;
	}
}

bool IsometricScene::onTouchBegan(Touch* touch, __unused Event* event)
{
	if (!_player->isMoving())
	{
		const Vec2& world_pos = _playerLayer->convertTouchToNodeSpace(touch);
		auto coord = tileFromPosition(world_pos);
		auto tile = _playerLayer->getTileAt(coord);

		if (tile != _player->getCurrentTile() && (tile->getLayerChange().z != 0 || tile->isWalkableInto()))
		{
			std::vector<SpriteTile*> path = findPath(_player->getCurrentTile()->getCoords(), tile->getCoords(), _playerLayer);

			if (path.empty())
				CCLOG("path could not be found");
			else
				_player->move(path);

			return true;
		}
	}
	return false;
}

void IsometricScene::onTouchEnded(__unused Touch* touch, __unused Event* event)
{
	// do nothing
}

Vec2 IsometricScene::tileFromPosition(const Vec2& pWorld)
{
	float wx = pWorld.x;
	float wy = pWorld.y;
	wx *= CC_CONTENT_SCALE_FACTOR();
	wy *= CC_CONTENT_SCALE_FACTOR();

	float tw = _map->getTileSize().width;
	float th = _map->getTileSize().height;
	float mw = _playerLayer->getLayerSize().width;
	float mh = _playerLayer->getLayerSize().height;

	float isoX = floorf(mh - wy / th + wx / tw - mw / 2);
	float isoY = floorf(mh - wy / th - wx / tw + mw / 2 - 1 / 2);

	return {isoX, isoY};
}

std::vector<SpriteTile*> IsometricScene::findPath(const Vec2& startCoord, const Vec2& targetCoord, SpriteLayer* layer)
{
	std::vector<SpriteTile*> open;
	std::vector<SpriteTile*> close;
	std::vector<SpriteTile*> children;

	SpriteTile* current = layer->getTileAt(startCoord);
	current->f = 0;
	open.push_back(current);

	while (!open.empty())
	{
		double lowestF = DBL_MAX;
		unsigned int lowestIndex = 0;
		for (unsigned int i = 0; i < open.size(); i++)
		{
			current = open.at(i);
			if (lowestF > current->f)
			{
				lowestIndex = i;
				lowestF = current->f;
			}
		}

		current = open.at(lowestIndex);
		children.clear();
		open.erase(open.begin() + lowestIndex);

		if (current->getCoords() != startCoord)
			close.push_back(current);

		children = layer->getSurroundingTiles(current->getCoords());
		for (SpriteTile* inAnalysis : children)
		{
			if (std::find(close.begin(), close.end(), inAnalysis) != close.end())
				continue;

			if (inAnalysis->getCoords() == targetCoord)
			{
				inAnalysis->prev = current;
				open.clear();
				close.clear();

				close.insert(close.begin(), inAnalysis);
				while (close.front()->getCoords() != startCoord)
					close.insert(close.begin(), close.front()->prev);
				close.erase(close.begin());
				break;
			}

			validateCandidateForOpenList(inAnalysis, current, targetCoord, open);
		}
	}

	if (close.back()->getCoords() != targetCoord)
		close.clear();

	children.clear();
	return close;
}

void IsometricScene::validateCandidateForOpenList(
	SpriteTile* inAnalysis, SpriteTile* current, const cocos2d::Vec2& targetCoord, std::vector<SpriteTile*>& open)
{
	if (inAnalysis->isWalkableInto() && inAnalysis->getLayerChange().z == 0)
	{
		Vec2 aux = current->getCoords();
		aux.subtract(inAnalysis->getCoords());
		const double g = std::abs(aux.x) + std::abs(aux.y);

		aux = targetCoord;
		aux.subtract(inAnalysis->getCoords());
		const double h = std::pow(aux.x, 2) + std::pow(aux.y, 2);

		const double f = g + h;

		auto openIt = std::find(open.begin(), open.end(), inAnalysis);
		if (openIt == open.end())
		{
			inAnalysis->f = f;
			inAnalysis->g = g;
			inAnalysis->prev = current;
			open.push_back(inAnalysis);
		}
		else if (g < open.at(openIt - open.begin())->g)
		{
			inAnalysis->f = f;
			inAnalysis->g = g;
			inAnalysis->prev = current;
		}
	}
}