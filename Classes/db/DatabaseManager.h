#ifndef H_DATABASE_MANAGER
#define H_DATABASE_MANAGER

#include "cocos2d.h"
#include "extensions/sqlite3.h"

class DatabaseManager
{
public:
	DatabaseManager() {};
	void openReadOnly();
	void open();
	void exec(const std::string& query, int (* callback)(void*, int, char**, char**));
	void close();
private:
	sqlite3* _pdb = nullptr;
	const char* _testQuery = "SELECT * FROM settings LIMIT 1";
	void initDB();
};

#endif //H_DATABASE_MANAGER
