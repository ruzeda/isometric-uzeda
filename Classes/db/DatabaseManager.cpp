#include "DatabaseManager.h"

USING_NS_CC;

void DatabaseManager::open()
{
	std::string path = FileUtils::getInstance()->getWritablePath() + "data.db";
	CCLOG("Located database: %s", path.c_str());

	FILE* file = fopen(path.c_str(), "r");
	if (file == nullptr) file = fopen(path.c_str(), "wb");
	fclose(file);

	int result = sqlite3_open(path.c_str(), &_pdb);

	char msg[255];
	if (result != SQLITE_OK)
	{
		sprintf(msg, "Could not open database connection: %s", sqlite3_errmsg(_pdb));
	}
	CCASSERT(result == 0, std::string(msg).c_str());

	// If no records are found we should initialize the database
	initDB();
}

void DatabaseManager::initDB()
{
	if (_pdb != nullptr)
	{
		sqlite3_stmt* query;
		int result = sqlite3_prepare_v2(_pdb, _testQuery, -1, &query, nullptr);

		if (result == SQLITE_OK)
		{
			result = sqlite3_step(query);
		}

		if (result != SQLITE_ROW)
		{
			CCLOG("Empty database found, initializing now");
			sqlite3_finalize(query);

			// open .sql script
			std::string path = FileUtils::getInstance()->fullPathForFilename("sql/schema.sql");
			std::string queries = FileUtils::getInstance()->getStringFromFile(path);
			exec(queries, nullptr);

			int result = sqlite3_prepare_v2(_pdb, _testQuery, -1, &query, nullptr);

			if (result == SQLITE_OK)
			{
				result = sqlite3_step(query);
			}

			if (result != SQLITE_ROW)
			{
				CCLOGERROR("Could not initialize the database.");
			}
		}
		else
		{
			CCLOG("Database already initialized");
		}

		sqlite3_finalize(query);
	}
}

void DatabaseManager::openReadOnly()
{
	std::string path = FileUtils::getInstance()->getWritablePath() + "data.db";
	CCLOG("Located database: %s", path.c_str());

	FILE* file = fopen(path.c_str(), "r");
	if (file == nullptr) file = fopen(path.c_str(), "wb");
	fclose(file);

	int result = sqlite3_open_v2(path.c_str(), &_pdb, SQLITE_OPEN_READONLY, nullptr);

	char msg[255];
	if (result != SQLITE_OK)
	{
		sprintf(msg, "Could not open database connection: %s", sqlite3_errmsg(_pdb));
	}
	CCASSERT(result == 0, std::string(msg).c_str());
}

void DatabaseManager::exec(const std::string& query, int (* callback)(void*, int, char**, char**))
{
	if (_pdb != nullptr)
	{
		char* msg = new char[255];
		sqlite3_exec(_pdb, query.c_str(), callback, nullptr, &msg);

		if (msg != nullptr)
		{
			CCLOGERROR("%s", msg);
			sqlite3_free(msg);
		}
	}
	else
	{
		CCLOGERROR("Database connection hasn't been opened");
	}
}

void DatabaseManager::close()
{
	if (_pdb != nullptr)
	{
		int result = sqlite3_close_v2(_pdb);

		char msg[255];
		if (result != SQLITE_OK)
		{
			sprintf(msg, "Could not close database connection: %s", sqlite3_errmsg(_pdb));
		}
		CCASSERT(result == 0, msg);
	}
}