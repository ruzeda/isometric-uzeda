#ifndef H_CONSTANTS
#define H_CONSTANTS

#include "cocos2d.h"

USING_NS_CC;

using namespace std;

static const uint TTF_FONT_SIZE = 20;
static const uint FONT_WIDTH = 16;
static const uint FONT_HEIGHT = 32;
static const uint PADDING = FONT_WIDTH >> 1u;
static const uint PADDING_HALVED = PADDING >> 1u;
static const char* const TTF_FONT_NAME = "fixedsys.ttf";
static const char* const BM_FONT_NAME = "fixedsys.fnt";

#endif // H_CONSTANTS
