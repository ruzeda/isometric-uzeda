#include "Constants.h"
#include "Dialog.h"

USING_NS_CC;

bool Dialog::init(const Size& preferredSize)
{
	if (!Scale9Sprite::initWithFile("dialog/dialog.png"))
	{
		return false;
	}

	setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

	_preferredSize.width = preferredSize.width;
	_preferredSize.height = preferredSize.height;
	setPreferredSize(
		Size(preferredSize.width * FONT_WIDTH + _insetLeft * 2, preferredSize.height * FONT_HEIGHT + _insetTop * 2));
	CCLOG("Calculated Dialog size: %.0f, %.0f", _contentSize.width, _contentSize.height);

	retain();
	return true;
}
