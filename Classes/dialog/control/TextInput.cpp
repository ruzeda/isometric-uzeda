#include "Constants.h"
#include "TextInput.h"

USING_NS_CC;

bool TextInput::init(const std::string& label, const float width)
{
	if (!Sprite::init())
	{
		return false;
	}

	setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	_width = width;

	_input = ui::TextField::create(label, TTF_FONT_NAME, TTF_FONT_SIZE);
	_input->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	_input->setColor(Color3B::BLACK);
	_input->setPosition(Vec2(PADDING + PADDING_HALVED, PADDING + PADDING_HALVED));
	addChild(_input);
	_input->setContentSize(Size(_width * FONT_WIDTH - PADDING, _input->getContentSize().height));
	_input->retain();

	_background = ui::Scale9Sprite::create("dialog/text_input.png");
	_background->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	float backgroundHeight = PADDING + _input->getContentSize().height;
	_background->setPreferredSize(
		Size(
			_width * ((static_cast<float>(TTF_FONT_SIZE) / FONT_HEIGHT) * TTF_FONT_SIZE), backgroundHeight));
	_background->setPosition(Vec2(PADDING, PADDING));
	addChild(_background);
	_background->retain();

	_label = Label::createWithBMFont(BM_FONT_NAME, label);
	_label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	_label->setColor(Color3B::BLACK);
	_label->setPosition(Vec2(PADDING, PADDING + PADDING + backgroundHeight));
	addChild(_label);
	_label->retain();

	_contentSize.setSize(
		_contentSize.width,
		(static_cast<uint>(PADDING) << 1u) + PADDING + _label->getContentSize().height + backgroundHeight);
	CCLOG("Calculated TextInput height is %.1f", _contentSize.height);
	retain();
	return true;
}
