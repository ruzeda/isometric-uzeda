#ifndef H_TEXT_INPUT
#define H_TEXT_INPUT

#include "cocos2d.h"
#include "ui/UIScale9Sprite.h"
#include "ui/UITextField.h"

class TextInput : public cocos2d::Sprite
{
public:
	TextInput() : Sprite() {};

	/**
	 * Initializes this TextInput with the given label positioned above a text field of the given width.
	 * @param label a string to be used as a label to this field.
	 * @param width the desired width in FONT_WIDTH units for the actual text field.
	 * @return true if the initialization doesn't throw any exceptions.
	 */
	bool init(const std::string& label, const float width);
protected:
	cocos2d::ui::TextField* _input = nullptr;
	cocos2d::Label* _label = nullptr;
	cocos2d::ui::Scale9Sprite* _background = nullptr;
	float _width;
};

#endif // H_TEXT_INPUT
