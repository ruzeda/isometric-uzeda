#include "Constants.h"
#include "IconLabel.h"

USING_NS_CC;

bool IconLabel::init(const std::string& icon, const std::string& label)
{
	if (!Sprite::init())
	{
		return false;
	}

	setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

	_icon = Sprite::create("icons/" + icon + ".png");
	_icon->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	_icon->setPosition(Vec2(PADDING, PADDING));
	addChild(_icon);
	_icon->retain();

	_label = Label::createWithBMFont(BM_FONT_NAME, label);
	_label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	_label->setColor(Color3B::BLACK);
	_label->setPosition(Vec2(PADDING * 2 + _label->getContentSize().height, PADDING));
	addChild(_label);
	_label->retain();

	_icon->setContentSize(Size(_label->getContentSize().height, _label->getContentSize().height));

	_contentSize.setSize(_contentSize.width, PADDING * 2 + _label->getContentSize().height);
	retain();
	return true;
}
