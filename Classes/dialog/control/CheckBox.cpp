#include "CheckBox.h"
#include "Constants.h"

USING_NS_CC;

bool CheckBox::init(const std::string& label, const bool displayInline)
{
	if (!Sprite::init())
	{
		return false;
	}

	setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

	_input = ui::CheckBox::create(
		"dialog/checkbox/background.png", "dialog/checkbox/background_pressed.png", "dialog/checkbox/check.png",
		"dialog/checkbox/background_disabled.png", "dialog/checkbox/check_disabled.png");
	_input->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	_input->setPosition(Vec2(PADDING, PADDING));
	addChild(_input);
	_input->retain();

	_label = Label::createWithBMFont(BM_FONT_NAME, label);
	_label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	_label->setColor(Color3B::BLACK);
	_label->setPosition(PADDING, _input->getPositionY() + _input->getContentSize().height + PADDING_HALVED);
	addChild(_label);
	_label->retain();

	_input->setPositionX((_label->getContentSize().width / 2) - (_input->getContentSize().width / 2) + PADDING);

	_contentSize.setSize(
		_contentSize.width, (static_cast<uint>(PADDING) << 1u) + PADDING_HALVED + _label->getContentSize().height +
			_input->getContentSize().height);
	retain();
	return true;
}
