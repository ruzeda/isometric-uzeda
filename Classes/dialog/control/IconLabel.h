#ifndef H_ICON_LABEL
#define H_ICON_LABEL

#include "cocos2d.h"

class IconLabel : public cocos2d::Sprite
{
public:
	IconLabel() : Sprite() {};

	/**
	 * Initializes this IconLabel with the given label positioned to the right of the given icon.
	 * @param icon the name of the icon under Resources/icons.
	 * @param label the label that will be positioned to the right of the icon.
	 * @return true if the initialization doesn't throw any exceptions.
	 */
	bool init(const std::string& icon, const std::string& label);
protected:
	cocos2d::Sprite* _icon = nullptr;
	cocos2d::Label* _label = nullptr;
};

#endif // H_ICON_LABEL
