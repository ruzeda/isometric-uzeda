#ifndef H_CHECK_BOX
#define H_CHECK_BOX

#include "cocos2d.h"
#include "ui/UICheckBox.h"

class CheckBox : public cocos2d::Sprite
{
public:
	CheckBox() : cocos2d::Sprite() {};

	/**
	 * Initializes this Checkbox with the given label positioned above or to the right of the actual checkbox depending
	 * on the value of displayInline.
	 * @param label a string to be used as a label to this field.
	 * @param displayInline whether to show the label above or to the right of the actual checkbox.
	 * @return true if the initialization doesn't throw any exceptions.
	 */
	bool init(const std::string& label, const bool displayInline);
protected:
	cocos2d::ui::CheckBox* _input = nullptr;
	cocos2d::Label* _label = nullptr;
};

#endif // H_CHECK_BOX
