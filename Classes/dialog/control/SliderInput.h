#ifndef H_SLIDER_INPUT
#define H_SLIDER_INPUT

#include "cocos2d.h"
#include "ui/UISlider.h"

class SliderInput : public cocos2d::Sprite
{
public:
	SliderInput() : cocos2d::Sprite() {};

	/**
	 * Initializes this SliderInput with the given label positioned above it.
	 * @param label a string to be used as a label to this field.
	 * @return true if the initialization doesn't throw any exceptions.
	 */
	bool init(const std::string& label, const float width);
protected:
	cocos2d::ui::Slider* _input = nullptr;
	cocos2d::Label* _label = nullptr;
	float _width;
};

#endif // H_SLIDER_INPUT
