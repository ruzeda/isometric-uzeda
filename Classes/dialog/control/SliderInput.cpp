#include "Constants.h"
#include "SliderInput.h"

USING_NS_CC;

bool SliderInput::init(const std::string& label, const float width)
{
	if (!Sprite::init())
	{
		return false;
	}

	setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	_width = width;

	_input = ui::Slider::create();
	_input->loadBarTexture("dialog/text_input.png");
	_input->loadProgressBarTexture("dialog/slider/bar.png");
	_input->loadSlidBallTextures(
		"dialog/slider/handle.png", "dialog/slider/handle_pressed.png", "dialog/slider/handle_disabled.png");
	_input->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	_input->setPosition(Vec2(PADDING, PADDING));
	addChild(_input);
	_input->setScale9Enabled(true);
	_input->setContentSize(Size(_width * FONT_WIDTH, _input->getContentSize().height));
	_input->retain();

	_label = Label::createWithBMFont(BM_FONT_NAME, label);
	_label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	_label->setColor(Color3B::BLACK);
	_label->setPosition(PADDING, _input->getPositionY() + _input->getContentSize().height + PADDING_HALVED);
	addChild(_label);
	_label->retain();

	_contentSize.setSize(
		_contentSize.width, (static_cast<uint>(PADDING) << 1u) + PADDING_HALVED + _label->getContentSize().height +
			_input->getContentSize().height);
	retain();
	return true;
}