#ifndef H_TEXT_DIALOG
#define H_TEXT_DIALOG

#include "cocos2d.h"
#include "Dialog.h"
#include "entity/Entity.h"

class TextDialog : public Dialog
{
public:
	TextDialog() : Dialog() {};
	bool init(const cocos2d::Size& size, const Entity* entity);
	void showText(const std::string& text);
	void updatePosition();
protected:
	const Entity* _entity = nullptr;
	std::vector<cocos2d::Vector<cocos2d::Label*>> _pages;
	uint _currentLetter = 0;
	uint _currentLine = 0;
	uint _currentPage = 0;

	void showNextLetter();
	cocos2d::Label* addLine(const std::string& line);
};

#endif // H_TEXT_DIALOG
