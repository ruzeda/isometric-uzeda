#include "Constants.h"
#include "TextDialog.h"

USING_NS_CC;

bool TextDialog::init(const Size& preferredSize, const Entity* entity)
{
	bool ret = Scale9Sprite::initWithFile("dialog/dialog.png");
	_entity = entity;
	setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);

	_preferredSize.width = preferredSize.width;
	_preferredSize.height = preferredSize.height;
	setPreferredSize(Size(preferredSize.width * 16 + _insetLeft * 2, preferredSize.height * 32 + _insetTop * 2));
	CCLOG("Calculated Dialog size: %.0f, %.0f", _contentSize.width, _contentSize.height);

	retain();
	return ret;
}

void TextDialog::showText(const std::string& text)
{
	const uint width = static_cast<int>(_preferredSize.width);
	const uint height = static_cast<int>(_preferredSize.height);
	uint nLines = static_cast<int>(text.length() / _preferredSize.width);
	if (text.length() % width != 0) nLines++;
	uint nPages = nLines / height;
	if (nLines % height != 0) nPages++;

	for (uint i = 0; i < nPages; i++)
	{
		Vector<Label*> page;
		for (uint j = 0; j < height; j++)
		{
			uint k = i * height * width; // prev pages
			k += j * width; // prev lines
			if (k < text.size())
			{
				auto line = addLine(text.substr(k, width));
				line->setPosition(
					_insetLeft, (_contentSize.height - _insetTop) - (line->getContentSize().height * static_cast<float>(j + 1)));
				page.pushBack(line);
			}
		}
		_pages.push_back(page);
	}

	showNextLetter();
}

Label* TextDialog::addLine(const std::string& line)
{
	auto label = Label::createWithBMFont(BM_FONT_NAME, line);
	label->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	addChild(label);
	for (uint k = 0; k < label->getStringLength(); k++)
	{
		label->getLetter(k)->setOpacity(0);
	}
	return label;
}

void TextDialog::showNextLetter()
{
	auto page = _pages.at(_currentPage);
	if (_currentLine < page.size())
	{
		auto line = page.at(_currentLine);
		if (_currentLetter < line->getStringLength())
		{
			auto letter = line->getLetter(_currentLetter++);
			auto callback = CallFunc::create(CC_CALLBACK_0(TextDialog::showNextLetter, this));
			letter->runAction(
				Sequence::create(Spawn::create(FadeIn::create(.1), TintTo::create(.1, 0, 0, 0), NULL), callback, NULL));
		}
		else
		{
			_currentLine++;
			_currentLetter = 0;
			showNextLetter();
		}
	}
	else if (_currentPage < _pages.size() - 1)
	{
		for (auto line : page)
		{
			for (uint i = 0; i < line->getStringLength(); i++)
			{
				line->getLetter(i)->setOpacity(0);
			}
		}
		_currentPage++;
		_currentLine = 0;
		showNextLetter();
	}
}

void TextDialog::updatePosition()
{
	Vec2 pos = _entity->getPosition();
	uint width = static_cast<uint>(_entity->getContentSize().width);
	uint height = static_cast<uint>(_entity->getContentSize().height);
	pos.add(Vec2(width >> 1u, (height >> 2u) * 3.f));
	width = static_cast<uint>(getContentSize().width);
	pos.subtract(Vec2(width >> 2u, 0));
	setPosition(pos);
}
