#include "DialogManager.h"

USING_NS_CC;

UIDialog* DialogManager::createUIDialog(const Size& preferredSize)
{
	CCLOG("Creating a new UI Dialog, %.0f, %.0f", preferredSize.width, preferredSize.height);
	auto dialog = new UIDialog();
	dialog->init(preferredSize);
	_dialogs.push_back(dialog);
	_container->addChild(dialog);
	return dialog;
}

TextDialog* DialogManager::createTextDialog(const Size& preferredSize, const Entity* entity)
{
	CCLOG("Creating a new Text Dialog, %.0f, %.0f for entity %s", preferredSize.width, preferredSize.height,
		entity->getIdentifier().c_str());
	auto dialog = new TextDialog();
	dialog->init(preferredSize, entity);
	_dialogs.push_back(dialog);
	_textDialogs.push_back(dialog);
	_container->addChild(dialog);

	Vec2 pos = entity->getPosition();
	uint width = static_cast<uint>(entity->getContentSize().width);
	uint height = static_cast<uint>(entity->getContentSize().height);
	pos.add(Vec2(width >> 1u, (height >> 2u) * 3.f));
	width = static_cast<uint>(dialog->getContentSize().width);
	pos.subtract(Vec2(width >> 2u, 0));
	dialog->setPosition(pos);
	return dialog;
}

void DialogManager::update()
{
	for (auto dialog : _textDialogs)
	{
		dialog->updatePosition();
	}
}
