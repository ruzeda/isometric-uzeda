#include "UIDialog.h"

USING_NS_CC;

void UIDialog::addChild(Sprite* node)
{
	Dialog::addChild(node);

	float y = _contentSize.height - _insetTop;
	if (_children.size() > 1)
	{
		auto lastChild = _children.at(_children.size() - 2);
		y = lastChild->getPositionY() - node->getContentSize().height;
	}
	y -= node->getContentSize().height;

	CCLOG("UIDialog's new node will be positioned at %.1f, %.1f", _insetLeft, y);
	node->setPosition(Vec2(_insetLeft, y));
}
