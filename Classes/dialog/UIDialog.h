#ifndef H_UI_DIALOG
#define H_UI_DIALOG

#include "cocos2d.h"
#include "Dialog.h"

class UIDialog : public Dialog
{
public:
	UIDialog() : Dialog() {};

	/**
	 * Adds a new control/input to this dialog positioning it vertically after its last child sprite.
	 * @param node the new control/input to be added.
	 */
	void addChild(cocos2d::Sprite* node);
};

#endif // H_UI_DIALOG
