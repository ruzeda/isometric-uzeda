#ifndef H_DIALOG_MANAGER
#define H_DIALOG_MANAGER

#include "cocos2d.h"
#include "entity/Entity.h"
#include "Dialog.h"
#include "TextDialog.h"
#include "UIDialog.h"

class DialogManager
{
public:
	/**
	 * Create a vertical layout dialog to hold UI controls/inputs.
	 * @param preferredSize the preferred size for the dialog in FONT_WIDTH units.
	 * @return a pointer to a UIDialog instance.
	 */
	UIDialog* createUIDialog(const cocos2d::Size& preferredSize);

	/**
	 * Create a dialog that will display text in pages and each character at a time.
	 * @param preferredSize the preferred size for the dialog in FONT_WIDTH units.
	 * @param entity an entity above which the dialog will be positioned.
	 * @return a pointer to a TextDialog instance.
	 */
	TextDialog* createTextDialog(const cocos2d::Size& preferredSize, const Entity* entity);

	/**
	 * Updates all managed TextDialog's position to the respective entity's position.
	 */
	void update();

	DialogManager(cocos2d::Node* container) { _container = container; }
	const std::vector<Dialog*>& getDialogs() const { return _dialogs; }

protected:
	cocos2d::Node* _container = nullptr;
	std::vector<Dialog*> _dialogs;
	std::vector<TextDialog*> _textDialogs;
};

#endif // H_DIALOG_MANAGER