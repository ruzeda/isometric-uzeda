#ifndef H_DIALOG
#define H_DIALOG

#include "cocos2d.h"
#include "ui/UIScale9Sprite.h"

class Dialog : public cocos2d::ui::Scale9Sprite
{
public:
	Dialog() : cocos2d::ui::Scale9Sprite() {};

	/**
	 * Initializes this dialog instance with a given preferred size in FONT_WIDTH units.
	 * @param size the preferred size in FONT_WIDTH units.
	 * @return true if the initialization doesn't throw any exceptions.
	 */
	virtual bool init(const cocos2d::Size& size);

	cocos2d::Size& getPreferredSize() { return _preferredSize; }

protected:
	cocos2d::Size _preferredSize;
};

#endif // H_DIALOG
