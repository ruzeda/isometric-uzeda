#ifndef H_SPRITE_BATCH_LAYER
#define H_SPRITE_BATCH_LAYER

#include "cocos2d.h"

class IsometricMap;

class SpriteBatchLayer : public cocos2d::TMXLayer
{
public:
	SpriteBatchLayer();
	bool init(
		cocos2d::TMXTilesetInfo* tileSetInfo, cocos2d::TMXLayerInfo* layerInfo, IsometricMap* map,
		cocos2d::TMXMapInfo* mapInfo);

	IsometricMap* getMap() const { return _map; }

	bool isWalkableOver(const int tileId);
	short getTileIdAt(const cocos2d::Vec2& coord);

protected:
	std::vector<std::vector<int>> _tilesIds;
	IsometricMap* _map = nullptr;
	std::vector<bool> _walkableOverFlags;
	std::vector<bool> _walkableIntoFlags;
};

#endif // H_SPRITE_BATCH_LAYER