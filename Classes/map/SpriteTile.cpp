#include "SpriteTile.h"
#include "IsometricScene.h"
#include "IsometricMap.h"
#include "SpriteLayer.h"
#include "SpriteBatchLayer.h"
#include "base/ccTypes.h"

USING_NS_CC;

SpriteTile::SpriteTile() : Sprite() {}

bool SpriteTile::init(
	const int tileId, SpriteLayer* layer, const int col, const int row, bool walkableOver, bool walkableInto)
{
	int gid = tileId == 0 ? 1 : tileId;

	_layer = layer;
	_col = col;
	_row = row;
	_coords = cocos2d::Vec2(static_cast<float>(_col), static_cast<float>(_row));
	_tileId = tileId;

	Rect const rectPixels = _layer->getTileSet()->getRectForGID(gid);
	Rect const rect = CC_RECT_PIXELS_TO_POINTS(rectPixels);

	if (tileId == 0)
	{
		if (!Sprite::init())
		{
			return false;
		}
		setAnchorPoint(Vec2::ANCHOR_MIDDLE);
		setContentSize(Size(rect.size.width, rect.size.height));
	}
	else
	{
		if (!initWithTexture(_layer->getTextureAtlas()->getTexture(), rect))
		{
			return false;
		}
		Texture2D::TexParams texParams = backend::SamplerDescriptor(
			backend::SamplerFilter::NEAREST, backend::SamplerFilter::NEAREST, backend::SamplerAddressMode::CLAMP_TO_EDGE,
			backend::SamplerAddressMode::CLAMP_TO_EDGE);
		getTexture()->setTexParameters(texParams);
	}

	setupWalkableFlags(walkableInto, walkableOver);

	/*auto anchorPoint11 = DrawNode::create();
	anchorPoint11->drawDot(Point(
		getContentSize().width * _anchorPoint.x,
		getContentSize().height * _anchorPoint.y),
		3, Color4F(1.0, 0.0, 0.0, 1.0));
	addChild(anchorPoint11);*/
	retain();
	return true;
}

void SpriteTile::setupWalkableFlags(bool walkableInto, bool walkableOver)
{
	int groundTileId;
	const int groundLocalZOrder = _layer->getMap()->getChildren().size() - 1;
	bool groundWalkable = false;
	const Vec2& groundCoord = Vec2(static_cast<float>(_col) + 1, static_cast<float>(_row) + 1);

	if (groundLocalZOrder >= 0 && groundCoord.x < _layer->getLayerSize().width &&
		groundCoord.y < _layer->getLayerSize().height)
	{
		Node* groundNode = _layer->getMap()->getChildren().at(groundLocalZOrder);
		auto groundSprite = dynamic_cast<SpriteLayer*>(groundNode);
		if (groundSprite == nullptr)
		{
			auto groundSpriteBatch = dynamic_cast<SpriteBatchLayer*>(groundNode);
			groundTileId = groundSpriteBatch->getTileIdAt(groundCoord);
			groundWalkable = groundTileId != 0 && groundSpriteBatch->isWalkableOver(groundTileId);
		}
		else
		{
			groundTileId = groundSprite->getTileIdAt(groundCoord);
			groundWalkable = groundTileId != 0 && groundSprite->getTileAt(groundCoord)->isWalkableOver();
		}
	}
	_walkableInto = walkableInto && groundWalkable;
	_walkableOver = walkableOver;
}

Vec2 SpriteTile::getSpriteAnchor() const
{
	Vec2 pos = getPosition();
	pos.add(Vec2(getContentSize().width / 2, getContentSize().height / 4));
	return pos;
}