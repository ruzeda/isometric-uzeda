#ifndef H_SPRITE_TILE
#define H_SPRITE_TILE

#include "cocos2d.h"

class SpriteLayer;

class SpriteTile : public cocos2d::Sprite
{
public:
	SpriteTile();
	bool init(
		const int tileId, SpriteLayer* layer, const int col, const int row, bool walkableOver, bool walkableInto);

	SpriteLayer* getLayer() const noexcept { return _layer; }

	cocos2d::Vec2 getCoords() const noexcept { return _coords; }

	int getTileId() const noexcept { return _tileId; }

	cocos2d::Vec2 getSpriteAnchor() const;

	bool isWalkableOver() const { return _walkableOver; }

	bool isWalkableInto() const { return _walkableInto; }

	void setLayerChange(cocos2d::Vec3 coords) { _layerChange = coords; }

	cocos2d::Vec3 getLayerChange() const { return _layerChange; }

	// TODO Move this to SpriteLayer
	double f = 0;
	double g = 0;
	SpriteTile* prev = nullptr;

protected:
	SpriteLayer* _layer = nullptr;
	int _col = 0;
	int _row = 0;
	cocos2d::Vec2 _coords = cocos2d::Vec2::ZERO;
	int _tileId = 0;

	void setupWalkableFlags(bool walkableInto, bool walkableOver);

	// Move this to SpriteLayer
	bool _walkableOver = true;
	bool _walkableInto = true;
	cocos2d::Vec3 _layerChange = cocos2d::Vec3::ZERO;
};

#endif // H_SPRITE_TILE