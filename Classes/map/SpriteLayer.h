#ifndef H_SPRITE_LAYER
#define H_SPRITE_LAYER

#include "cocos2d.h"

class Entity;

class IsometricMap;

class SpriteTile;

class SpriteLayer : public cocos2d::Node
{
public:
	SpriteLayer();
	~SpriteLayer();
	bool init(
		cocos2d::TMXTilesetInfo* tileSetInfo, cocos2d::TMXLayerInfo* layerInfo, IsometricMap* map,
		cocos2d::TMXMapInfo* mapInfo);
	int getTileIdAt(const cocos2d::Vec2& coord);
	SpriteTile* getTileAt(const cocos2d::Vec2& coord);
	void addEntity(Entity* entity, const cocos2d::Vec2& coord);
	std::vector<SpriteTile*> getSurroundingTiles(const cocos2d::Vec2& coord);

	std::string getLayerName() const { return _layerName; }

	cocos2d::Size getLayerSize() const noexcept { return _layerSize; }

	IsometricMap* getMap() const noexcept { return _map; }

	cocos2d::TMXTilesetInfo* getTileSet() const noexcept { return _tileSet; }

	cocos2d::TextureAtlas* getTextureAtlas() const noexcept { return _textureAtlas; }

	std::vector<std::vector<SpriteTile*>> getTiles() const { return _tiles; }

protected:
	std::string _layerName = "";
	cocos2d::Size _layerSize = cocos2d::Size::ZERO;
	std::vector<std::vector<int>> _tilesIds;
	std::vector<std::vector<SpriteTile*>> _tiles;
	IsometricMap* _map = nullptr;
	cocos2d::TMXTilesetInfo* _tileSet = nullptr;
	cocos2d::TextureAtlas* _textureAtlas = nullptr;
	bool _useAutomaticVertexZ = false;
	int _vertexZValue = 0;
	cocos2d::TMXLayer* _tmxLayer = nullptr;

	void parseTileIds(cocos2d::TMXLayerInfo* layerInfo);
	void setupTiles();
	void setupTile(const int row, const int col, const int z);
	void parseLayerChanges(cocos2d::TMXLayerInfo* layerInfo);
};

#endif // H_SPRITE_LAYER