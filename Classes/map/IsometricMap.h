#include <base64.h>
#include <atitc.h>
#include <__bit_reference>

#ifndef H_ISOMETRIC_MAP
	#define H_ISOMETRIC_MAP

	#include "cocos2d.h"

class IsometricScene;

class SpriteLayer;

class SpriteBatchLayer;

class IsometricMap : public cocos2d::TMXTiledMap
{
public:
	IsometricMap() : TMXTiledMap() {};

	IsometricScene* getIsometricScene() const { return _isometricScene; }

	std::unordered_map<std::string, cocos2d::ValueMap> getMapProperties() const { return _mapProperties; }

	void init(const std::string& tmxFile, IsometricScene* scene);
	cocos2d::Node* getSpriteLayer(const std::string& layerName) const;
	cocos2d::Size getSize() const;
	cocos2d::ValueMap getPropertiesForGID(const std::string& GID);
	void updateVisibleLayers(__unused __unused SpriteLayer const* currentLayer);

protected:
	IsometricScene* _isometricScene = nullptr;
	__unused  int const _mapOrientation = cocos2d::TMXOrientationIso;
	void buildWithMapInfo(cocos2d::TMXMapInfo* mapInfo) override;
	std::unordered_map<std::string, cocos2d::ValueMap> _mapProperties;
};

#endif // H_ISOMETRIC_MAP