#include "SpriteLayer.h"
#include "IsometricMap.h"
#include "SpriteBatchLayer.h"
#include "SpriteTile.h"
#include "entity/Entity.h"
#include "player/Player.h"

USING_NS_CC;

SpriteLayer::SpriteLayer() : Node() {}

SpriteLayer::~SpriteLayer()
{
	CC_SAFE_DELETE(_tileSet);
}

bool SpriteLayer::init(
	cocos2d::TMXTilesetInfo* tileSetInfo, cocos2d::TMXLayerInfo* layerInfo, IsometricMap* map, TMXMapInfo* mapInfo)
{
	if (!Node::init())
	{
		return false;
	}

	CCLOG("Initializing %d SpriteLayer, and its CCTMXLayer", _ID);
	_tmxLayer = TMXLayer::create(tileSetInfo, layerInfo, mapInfo);

	Texture2D* texture = nullptr;
	if (tileSetInfo)
	{
		texture = Director::getInstance()->getTextureCache()->addImage(tileSetInfo->_sourceImage);
	}

	_layerSize = layerInfo->_layerSize;

	float totalNumberOfTiles = _layerSize.width * _layerSize.height;
	float capacity = totalNumberOfTiles * 0.35f + 1; // 35 percent is occupied ?

	_map = map;
	_tileSet = tileSetInfo;
	_tileSet->retain();

	//isometric conversion
	Vec2 offset = Vec2((map->getTileSize().width / 2) * (layerInfo->_offset.x - layerInfo->_offset.y),
		(map->getTileSize().height / 2) * (-layerInfo->_offset.x - layerInfo->_offset.y));
	setPosition(CC_POINT_PIXELS_TO_POINTS(offset));

	_textureAtlas = TextureAtlas::createWithTexture(texture, capacity);
	_layerName = layerInfo->_name;

	parseTileIds(layerInfo);
	setOpacity(layerInfo->_opacity);
	setupTiles();
	parseLayerChanges(layerInfo);
	retain();
	return true;
}

void SpriteLayer::parseTileIds(cocos2d::TMXLayerInfo* layerInfo)
{
	int layerWidth = static_cast<int>(_layerSize.width);
	int layerHeight = static_cast<int>(_layerSize.height);
	for (int y = 0; y < layerHeight; y++)
	{
		_tilesIds.emplace_back(std::vector<int>());
		for (int x = 0; x < layerWidth; x++)
		{
			int const pos = x + layerWidth * y;
			_tilesIds.at(y).emplace_back(layerInfo->_tiles[pos]);
		}
	}
}

void SpriteLayer::setupTiles()
{
	CCLOG("SpriteLayer isometric tiles setup");
	_tileSet->_imageSize = _textureAtlas->getTexture()->getContentSizeInPixels();

	int col;
	int row = 0;
	uint colThreshold = 2;
	int size = 0;
	int z = 0;
	_tiles.emplace_back(std::vector<SpriteTile*>());

	int layerWidth = static_cast<int>(_layerSize.width);
	int layerHeight = static_cast<int>(_layerSize.height);
	while (size < layerWidth)
	{
		if (colThreshold == 0)
		{
			row = 0;
			colThreshold = _tiles.at(row).size() + 1;
			if (colThreshold > layerWidth)
				colThreshold--;
			z++;
		}

		col = _tiles.at(row).size();
		if (col == colThreshold)
		{
			// row is complete, skip it
			if (row < layerHeight - 1)
				row++;
			continue;
		}

		while (col < colThreshold)
		{
			setupTile(row, col, z);
			col++;
		}

		colThreshold--;
		if (colThreshold > 0 && row < layerHeight - 1)
		{
			row++;
			if (row >= _tiles.size())
				_tiles.emplace_back(std::vector<SpriteTile*>());
		}

		size = _tiles.at(_tiles.size() - 1).size();
	}
}

void SpriteLayer::setupTile(const int row, const int col, const int z)
{
	const int tileId = _tilesIds.at(row).at(col);
	bool walkableOver = true;
	bool walkableInto = true;

	if (tileId != 0)
	{
		std::string aux = std::to_string(tileId);
		ValueMap properties = _map->getPropertiesForGID(aux);
		Value tileProperty = properties.at("walkableOver");
		walkableOver = tileProperty.asBool();
		tileProperty = properties.at("walkableInto");
		walkableInto = tileProperty.asBool();
	}

	auto tile = new SpriteTile();
	tile->init(tileId, this, col, row, walkableOver, walkableInto);
	_tmxLayer->setupTileSprite(tile, Vec2(static_cast<float>(col), static_cast<float>(row)), tileId);
	addChild(tile, z);
	_tiles.at(row).push_back(tile);
}

void SpriteLayer::addEntity(Entity* entity, const Vec2& coord)
{
	CCLOG("Adding entity to sprite layer %s at %.0f,%.0f", _layerName.c_str(), coord.x, coord.y);
	if (entity != nullptr)
	{
		if (dynamic_cast<Player*>(entity) != nullptr)
		{
			_map->updateVisibleLayers(this);
		}

		entity->setLayer(this);

		auto tile = getTileAt(coord);
		if (tile != nullptr)
		{
			entity->setCurrentTile(tile);
			entity->setPosition(tile->getSpriteAnchor());
			entity->setLocalZOrder(tile->getLocalZOrder() + getLayerSize().width);
			entity->setStartPosition(tile->getSpriteAnchor());
			addChild(entity);
			entity->updateLocalZOrder(tile);
		}
	}
}

void SpriteLayer::parseLayerChanges(TMXLayerInfo* layerInfo)
{
	for (std::pair<std::string, Value> property : layerInfo->getProperties())
	{
		std::string aux;
		std::istringstream inputStream;

		inputStream.str(property.first);
		std::getline(inputStream, aux, ',');
		const int col = std::stoi(aux);
		std::getline(inputStream, aux, ',');
		const int row = std::stoi(aux);

		inputStream.clear();
		inputStream.str(property.second.asString());
		std::getline(inputStream, aux, ';');
		const int z = std::stoi(aux);

		std::getline(inputStream, aux, ';');
		const int x = std::stoi(aux);

		std::getline(inputStream, aux, ';');
		const int y = std::stoi(aux);

		_tiles.at(row).at(col)->setLayerChange(Vec3(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z)));
	}
}

int SpriteLayer::getTileIdAt(const Vec2& coord)
{
	const Vec2& rounded = Vec2(floor(coord.x), floor(coord.y));
	if (!_tilesIds.empty() && rounded.x < _layerSize.width && rounded.y < _layerSize.height && rounded.x >= 0 &&
		rounded.y >= 0)
		return _tilesIds.at(rounded.y).at(rounded.x);
	else
		return 0;
}

SpriteTile* SpriteLayer::getTileAt(const Vec2& coord)
{
	const Vec2& rounded = Vec2(floor(coord.x), floor(coord.y));
	if (!_tiles.empty() && rounded.x < _layerSize.width && rounded.y < _layerSize.height && rounded.x >= 0 &&
		rounded.y >= 0)
		return _tiles.at(rounded.y).at(rounded.x);
	else
		return nullptr;
}

std::vector<SpriteTile*> SpriteLayer::getSurroundingTiles(const Vec2& coord)
{
	std::vector<SpriteTile*> surrounds;

	if (getTileAt(coord))
	{
		if (coord.x > 0 && getTileAt(Vec2(coord.x - 1, coord.y)))
			surrounds.push_back(getTileAt(Vec2(coord.x - 1, coord.y)));
		if (coord.x < getLayerSize().width - 1 && getTileAt(Vec2(coord.x + 1, coord.y)))
			surrounds.push_back(getTileAt(Vec2(coord.x + 1, coord.y)));

		if (coord.y > 0 && getTileAt(Vec2(coord.x, coord.y - 1)))
			surrounds.push_back(getTileAt(Vec2(coord.x, coord.y - 1)));
		if (coord.y < getLayerSize().height - 1 && getTileAt(Vec2(coord.x, coord.y + 1)))
			surrounds.push_back(getTileAt(Vec2(coord.x, coord.y + 1)));

		if (coord.x > 0 && coord.y > 0 && getTileAt(Vec2(coord.x - 1, coord.y - 1)))
			surrounds.push_back(getTileAt(Vec2(coord.x - 1, coord.y - 1)));
		if (coord.x < getLayerSize().width - 1 && coord.y < getLayerSize().height - 1 &&
			getTileAt(Vec2(coord.x + 1, coord.y + 1)))
			surrounds.push_back(getTileAt(Vec2(coord.x + 1, coord.y + 1)));

		if (coord.x > 0 && coord.y < getLayerSize().height - 1 && getTileAt(Vec2(coord.x - 1, coord.y + 1)))
			surrounds.push_back(getTileAt(Vec2(coord.x - 1, coord.y + 1)));
		if (coord.x < getLayerSize().width - 1 && coord.y > 0 && getTileAt(Vec2(coord.x + 1, coord.y - 1)))
			surrounds.push_back(getTileAt(Vec2(coord.x + 1, coord.y - 1)));
	}

	return surrounds;
}