#include "SpriteBatchLayer.h"
#include "IsometricMap.h"

USING_NS_CC;

SpriteBatchLayer::SpriteBatchLayer() : TMXLayer() {}

bool SpriteBatchLayer::init(
	cocos2d::TMXTilesetInfo* tileSetInfo, cocos2d::TMXLayerInfo* layerInfo, IsometricMap* map, TMXMapInfo* mapInfo)
{
	if (layerInfo == nullptr || !initWithTilesetInfo(tileSetInfo, layerInfo, mapInfo))
	{
		return false;
	}

	CCLOG("Initializing %d SpriteBatchLayer : CCTMXLayer %s", _ID, layerInfo->_name.c_str());
	CCLOG("Parsing SpriteBatchLayer tiles");
	setupTiles();

	_map = map;

	int layerWidth = static_cast<int>(_layerSize.width);
	int layerHeight = static_cast<int>(_layerSize.height);
	for (int y = 0; y < layerHeight; y++)
	{
		_tilesIds.emplace_back(std::vector<int>());
		for (int x = 0; x < layerWidth; x++)
		{
			const int pos = x + layerWidth * y;
			int tileId = layerInfo->_tiles[pos];
			_tilesIds.at(y).emplace_back(tileId);

			if (tileId != 0)
			{
				ValueMap tileProperty = map->getPropertiesForGID(std::to_string(tileId));
				Value walkableProperty = tileProperty.at("walkableOver");
				_walkableOverFlags.push_back(walkableProperty.asBool());
				walkableProperty = tileProperty.at("walkableInto");
				_walkableIntoFlags.push_back(walkableProperty.asBool());
			}
		}
	}

	CCLOG("Parsing SpriteBatchLayer properties");
	for (std::pair<std::string, Value> property : layerInfo->getProperties())
		_properties.insert(property);

	retain();
	return true;
}

short SpriteBatchLayer::getTileIdAt(const Vec2& coord)
{
	const Vec2& rounded = Vec2(floor(coord.x), floor(coord.y));
	if (!_tilesIds.empty() && rounded.x < _layerSize.width && rounded.y < _layerSize.height && rounded.x >= 0 &&
		rounded.y >= 0)
		return _tilesIds.at(rounded.y).at(rounded.x);
	else
		return 0;
}

bool SpriteBatchLayer::isWalkableOver(const int tileId)
{
	if (tileId > 0 && tileId <= _walkableOverFlags.size())
		return _walkableOverFlags.at(tileId - 1);
	else
		return false;
}