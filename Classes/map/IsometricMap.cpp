#include "IsometricMap.h"
#include "SpriteLayer.h"
#include "SpriteBatchLayer.h"

USING_NS_CC;

void IsometricMap::init(const std::string& tmxFile, IsometricScene* scene)
{
	_isometricScene = scene;
	initWithTMXFile(tmxFile);
	setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
	setPosition(Vec2::ZERO);
	retain();
}

void IsometricMap::buildWithMapInfo(cocos2d::TMXMapInfo* mapInfo)
{
	CCLOG("Parsing tmx attributes");
	_mapSize = mapInfo->getMapSize();
	_tileSize = mapInfo->getTileSize();
	_objectGroups = mapInfo->getObjectGroups();
	_properties = mapInfo->getProperties();
	_tileProperties = mapInfo->getTileProperties();
	Value* value;

	CCLOG("Parsing tmx properties");
	for (int i = 0; i <= _tileProperties.size(); i++)
	{
		if (_tileProperties.find(i) != _tileProperties.end())
		{
			value = &_tileProperties.at(i);
			if (value != nullptr)
				_mapProperties[std::to_string(i)] = value->asValueMap();
		}
	}

	CCLOG("Parsing tmx layers");
	int z = 0;
	auto layers = mapInfo->getLayers();
	for (auto layerInfo : layers)
	{
		if (layerInfo->_visible)
		{
			CCLOG("- Layer: %s", layerInfo->_name.c_str());
			TMXTilesetInfo* tileset = tilesetForLayer(layerInfo, mapInfo);

			Size childSize;
			if (layerInfo->_name.find("batch") != std::string::npos)
			{
				auto batchLayer = new SpriteBatchLayer();
				batchLayer->init(tileset, layerInfo, this, mapInfo);
				batchLayer->setLocalZOrder(z);
				addChild(batchLayer, z);
				childSize = batchLayer->getContentSize();
			}
			else
			{
				auto spriteLayer = new SpriteLayer();
				spriteLayer->init(tileset, layerInfo, this, mapInfo);
				spriteLayer->setLocalZOrder(z);
				addChild(spriteLayer, z);
				childSize = spriteLayer->getContentSize();
			}

			layerInfo->_ownTiles = false;

			z++;
			Size currentSize = this->getContentSize();
			currentSize.width = std::max(currentSize.width, childSize.width);
			currentSize.height = std::max(currentSize.height, childSize.height);
			setContentSize(currentSize);
		}
	}
	_tmxLayerNum = z;
}

ValueMap IsometricMap::getPropertiesForGID(const std::string& GID)
{
	if (_mapProperties.find(GID) != _mapProperties.end())
		return _mapProperties.at(GID);
	else
		return ValueMap();
}

Node* IsometricMap::getSpriteLayer(const std::string& layerName) const
{
	CCASSERT(!layerName.empty(), "Invalid layer name!");

	for (auto& child : _children)
	{
		if (layerName.find("batch") != std::string::npos)
		{
			auto layer = dynamic_cast<SpriteBatchLayer*>(child);
			if (layer != nullptr && layerName == layer->getLayerName())
				return layer;
		}
		else
		{
			auto layer = dynamic_cast<SpriteLayer*>(child);
			if (layer != nullptr && layerName == layer->getLayerName())
				return layer;
		}
	}

	return nullptr;
}

Size IsometricMap::getSize() const
{
	float const width = getTileSize().width * getMapSize().width;
	float height = getTileSize().height * getMapSize().height;
	height /= 2;
	return Size(width, height);
}

void IsometricMap::updateVisibleLayers(__unused SpriteLayer const* currentLayer)
{
	/*int index = currentLayer->getLocalZOrder() < short_MAX ? 0 : currentLayer->getLocalZOrder();

	Node* layer = nullptr;
	while (index <= currentLayer->getLocalZOrder() && index < _children.size())
	{
		layer = _children.at(index);
		if (layer != nullptr)
			layer->setVisible(true);
		index++;
	}

	if (index < _children.size())
	{
		layer = _children.at(index);
		if (layer != nullptr)
			layer->setVisible(dynamic_cast<SpriteBatchLayer*>(layer) != nullptr);
	}*/
}