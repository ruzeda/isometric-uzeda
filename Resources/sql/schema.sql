
CREATE TABLE "settings" (
	"volume_bg" INTEGER NOT NULL DEFAULT 8,
	"volume_sfx" INTEGER NOT NULL DEFAULT 8,
	"volume_voice" INTEGER NOT NULL DEFAULT 10,
	"text_speed" INTEGER NOT NULL DEFAULT 5
);
INSERT INTO "settings" ("volume_bg", "volume_sfx", "volume_voice", "text_speed") VALUES (8, 8, 10, 5);

CREATE TABLE "scene" (
	"name" TEXT NOT NULL
);

CREATE TABLE "npc" (
	"name" TEXT NOT NULL,
	"scene" INTEGER NOT NULL,
	"pos_x" INTEGER NOT NULL,
	"pos_y" INTEGER NOT NULL,
	"pos_z" INTEGER NOT NULL
);
CREATE INDEX "npc_scene_index" ON "npc" ("scene");

CREATE TABLE "key_event" (
	"description" TEXT,
	"npc" INTEGER,
	"progress" INTEGER NOT NULL DEFAULT 0
);
CREATE INDEX "key_event_npc_index" ON "key_event" ("npc");